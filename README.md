# Combat-Craft
Combat Craft is a 3D open world game that is similar to the game Minecraft.
The documentation of the project included in the Project Documentation pdf file.

The Settings.cpp file handle the settings of the game.

# Images
![image1](https://raw.githubusercontent.com/omereli10/Combat-Craft/master/Documentation/Images/image1.png)

![image2](https://raw.githubusercontent.com/omereli10/Combat-Craft/master/Documentation/Images/image2.png)

![image3](https://raw.githubusercontent.com/omereli10/Combat-Craft/master/Documentation/Images/image3.png)

![image4](https://raw.githubusercontent.com/omereli10/Combat-Craft/master/Documentation/Images/image4.png)

![image5](https://raw.githubusercontent.com/omereli10/Combat-Craft/master/Documentation/Images/image5.png)

![image6](https://raw.githubusercontent.com/omereli10/Combat-Craft/master/Documentation/Images/image6.png)
