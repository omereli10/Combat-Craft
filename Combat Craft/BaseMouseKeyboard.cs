﻿using Microsoft.Xna.Framework.Input;

namespace Combat_Craft
{
    abstract class BaseMouseKeyboard
    {
        #region Data

        public int mouseWheel_Value;
        public int previousMouseWheel_Determine;
        public bool isHolding_MouseLeftButton;
        public bool isHolding_MouseRightButton;
        public bool isHolding_KeyboardFlyingMode;
        public bool isHolding_KeyboardMouseLock;
        public bool isHolding_KeyboardScrollUp;
        public bool isHolding_KeyboardScrollDown;
        public MouseState currentMouseState;
        public MouseState previosMouseState;

        #endregion Data

        #region Operations

        public abstract bool IsWalkRight();
        public abstract bool IsWalkLeft();
        public abstract bool IsWalkForward();
        public abstract bool IsWalkBackward();
        public abstract bool IsWalkUp();
        public abstract bool IsWalkDown();
        public abstract bool IsRunRight();
        public abstract bool IsRunLeft();
        public abstract bool IsRunForward();
        public abstract bool IsRunBackward();
        public abstract bool IsRunUp();
        public abstract bool IsRunDown();
        public abstract bool IsSlowdownRight();
        public abstract bool IsSlowdownLeft();
        public abstract bool IsSlowdownForward();
        public abstract bool IsSlowdownBackward();
        public abstract bool IsSlowdownUp();
        public abstract bool IsSlowdownDown();
        public abstract bool IsFlyingMode();
        public abstract bool IsJump();
        public abstract bool IsMouseLock();
        public abstract bool IsScrollUp();
        public abstract bool IsScrollDown();
        public abstract bool IsPressingEscape();
        public abstract bool IsNotPressingEscape();
        public abstract bool IsEasterEgg_o();
        public abstract bool IsEasterEgg_m();
        public abstract bool IsEasterEgg_e();
        public abstract bool IsEasterEgg_r();
        public abstract bool IsEasterEgg_l();
        public abstract bool IsEasterEgg_i();
        public abstract bool IsEasterEgg_1();
        public abstract bool IsEasterEgg_0();

        #endregion Operations
    }

    class UserKeyboard : BaseMouseKeyboard
    {
        #region Keys

        public Keys Right { get; private set; }
        public Keys Left { get; private set; }
        public Keys Forward { get; private set; }
        public Keys Backward { get; private set; }
        public Keys Up { get; private set; }
        public Keys Down { get; private set; }
        public Keys Run_Boost { get; private set; }
        public Keys Slowdown { get; private set; }
        public Keys FlyingMode { get; private set; }
        public Keys Jump { get; private set; }
        public Keys MouseLock { get; private set; }
        public Keys ScrollUp { get; private set; }
        public Keys ScrollDown { get; private set; }
        public Keys Escape { get; private set; }
        public Keys EasterEgg_o { get; private set; }
        public Keys EasterEgg_m { get; private set; }
        public Keys EasterEgg_e { get; private set; }
        public Keys EasterEgg_r { get; private set; }
        public Keys EasterEgg_l { get; private set; }
        public Keys EasterEgg_i { get; private set; }
        public Keys EasterEgg_1 { get; private set; }
        public Keys EasterEgg_0 { get; private set; }

        #endregion Keys

        #region Constractors

        public UserKeyboard(Keys right, Keys left, Keys forward, Keys backward, Keys up, 
                            Keys down, Keys run_boost, Keys slowdown, Keys flyingMode, Keys jump, 
                            Keys mouseLock, Keys scrollUp, Keys scrollDown, Keys easterEgg_o, 
                            Keys easterEgg_m, Keys easterEgg_e, Keys easterEgg_r, Keys easterEgg_l, Keys easterEgg_i, 
                            Keys easterEgg_1, Keys easterEgg_0, Keys escape)
        {
            this.Right = right;
            this.Left = left;
            this.Forward = forward;
            this.Backward = backward;
            this.Up = up;
            this.Down = down;
            this.Run_Boost = run_boost;
            this.Slowdown = slowdown;
            this.FlyingMode = flyingMode;
            this.Jump = jump;
            this.MouseLock = mouseLock;
            this.ScrollUp = scrollUp;
            this.ScrollDown = scrollDown;
            this.EasterEgg_o = easterEgg_o;
            this.EasterEgg_m = easterEgg_m;
            this.EasterEgg_e = easterEgg_e;
            this.EasterEgg_r = easterEgg_r;
            this.EasterEgg_l = easterEgg_l;
            this.EasterEgg_i = easterEgg_i;
            this.EasterEgg_1 = easterEgg_1;
            this.EasterEgg_0 = easterEgg_0;
            this.Escape = escape;

            this.mouseWheel_Value = 0;
            this.isHolding_MouseLeftButton = false;
            this.isHolding_MouseRightButton = false;
            this.isHolding_KeyboardFlyingMode = false;
        }

        #endregion Constractors

        #region KeysStates

        public override bool IsWalkRight()
        {
            return Keyboard.GetState().IsKeyDown(Right) && !(Keyboard.GetState().IsKeyDown(Run_Boost) && !(Keyboard.GetState().IsKeyDown(Slowdown)));
        }
        public override bool IsWalkLeft()
        {
            return Keyboard.GetState().IsKeyDown(Left) && !(Keyboard.GetState().IsKeyDown(Run_Boost)) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsWalkForward()
        {
            return Keyboard.GetState().IsKeyDown(Forward) && !Keyboard.GetState().IsKeyDown(Run_Boost) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsWalkBackward()
        {
            return Keyboard.GetState().IsKeyDown(Backward) && !(Keyboard.GetState().IsKeyDown(Run_Boost)) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsWalkUp()
        {
            return Keyboard.GetState().IsKeyDown(Up) && !(Keyboard.GetState().IsKeyDown(Run_Boost)) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsWalkDown()
        {
            return Keyboard.GetState().IsKeyDown(Down) && !(Keyboard.GetState().IsKeyDown(Run_Boost)) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsRunRight()
        {
            return Keyboard.GetState().IsKeyDown(Right) && Keyboard.GetState().IsKeyDown(Run_Boost) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsRunLeft()
        {
            return Keyboard.GetState().IsKeyDown(Left) && Keyboard.GetState().IsKeyDown(Run_Boost) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsRunForward()
        {
            return Keyboard.GetState().IsKeyDown(Forward) && (Keyboard.GetState().IsKeyDown(Run_Boost)) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsRunBackward()
        {
            return Keyboard.GetState().IsKeyDown(Backward) && (Keyboard.GetState().IsKeyDown(Run_Boost)) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsRunUp()
        {
            return Keyboard.GetState().IsKeyDown(Up) && Keyboard.GetState().IsKeyDown(Run_Boost) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsRunDown()
        {
            return Keyboard.GetState().IsKeyDown(Down) && Keyboard.GetState().IsKeyDown(Run_Boost) && !(Keyboard.GetState().IsKeyDown(Slowdown));
        }
        public override bool IsSlowdownRight()
        {
            return Keyboard.GetState().IsKeyDown(Right) && Keyboard.GetState().IsKeyDown(Slowdown) && !Keyboard.GetState().IsKeyDown(Run_Boost);
        }
        public override bool IsSlowdownLeft()
        {
            return Keyboard.GetState().IsKeyDown(Left) && Keyboard.GetState().IsKeyDown(Slowdown) && !Keyboard.GetState().IsKeyDown(Run_Boost);
        }
        public override bool IsSlowdownForward()
        {
            return Keyboard.GetState().IsKeyDown(Forward) && Keyboard.GetState().IsKeyDown(Slowdown) && !Keyboard.GetState().IsKeyDown(Run_Boost);
        }
        public override bool IsSlowdownBackward()
        {
            return Keyboard.GetState().IsKeyDown(Backward) && Keyboard.GetState().IsKeyDown(Slowdown) && !Keyboard.GetState().IsKeyDown(Run_Boost);
        }
        public override bool IsSlowdownUp()
        {
            return Keyboard.GetState().IsKeyDown(Up) && Keyboard.GetState().IsKeyDown(Slowdown) && !Keyboard.GetState().IsKeyDown(Run_Boost);
        }
        public override bool IsSlowdownDown()
        {
            return Keyboard.GetState().IsKeyDown(Down) && Keyboard.GetState().IsKeyDown(Slowdown) && !Keyboard.GetState().IsKeyDown(Run_Boost);
        }
        public override bool IsFlyingMode()
        {
            return Keyboard.GetState().IsKeyDown(FlyingMode);
        }
        public override bool IsJump()
        {
            return Keyboard.GetState().IsKeyDown(Jump);
        }
        public override bool IsMouseLock()
        {
            return Keyboard.GetState().IsKeyDown(MouseLock);
        }
        public override bool IsScrollUp()
        {
            return Keyboard.GetState().IsKeyDown(ScrollUp);
        }
        public override bool IsScrollDown()
        {
            return Keyboard.GetState().IsKeyDown(ScrollDown);
        }
        public override bool IsPressingEscape()
        {
            return Keyboard.GetState().IsKeyDown(Escape);
        }
        public override bool IsNotPressingEscape()
        {
            return Keyboard.GetState().IsKeyUp(Escape);
        }
        public override bool IsEasterEgg_o()
        {
            return Keyboard.GetState().IsKeyDown(EasterEgg_o);
        }
        public override bool IsEasterEgg_m()
        {
            return Keyboard.GetState().IsKeyDown(EasterEgg_m);
        }
        public override bool IsEasterEgg_e()
        {
            return Keyboard.GetState().IsKeyDown(EasterEgg_e);
        }
        public override bool IsEasterEgg_r()
        {
            return Keyboard.GetState().IsKeyDown(EasterEgg_r);
        }
        public override bool IsEasterEgg_l()
        {
            return Keyboard.GetState().IsKeyDown(EasterEgg_l);
        }
        public override bool IsEasterEgg_i()
        {
            return Keyboard.GetState().IsKeyDown(EasterEgg_i);
        }
        public override bool IsEasterEgg_1()
        {
            return Keyboard.GetState().IsKeyDown(EasterEgg_1);
        }
        public override bool IsEasterEgg_0()
        {
            return Keyboard.GetState().IsKeyDown(EasterEgg_0);
        }

        #endregion KeysStates
    }
}