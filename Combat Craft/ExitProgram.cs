﻿using Microsoft.Xna.Framework;
using System;

namespace Combat_Craft
{
    static class ExitProgram
    {
        #region Data

        private static Game game;
        private static Player player;
        private static bool[] lastKeysPressedMatch;
        private static Func<bool>[] keys_actions_to_exit;
        private static TimeSpan last_key_press_time;
        const int VALID_MS_BETWEEN_KEY_PRESS = 500;

        #endregion Data

        #region Constructors

        public static void Initialize(Game game_arg, Player player_arg)
        {
            game = game_arg;
            player = player_arg;
            keys_actions_to_exit = new Func<bool>[] { player.baseMouseKeyboard.IsPressingEscape,
                                                      player.baseMouseKeyboard.IsNotPressingEscape,
                                                      player.baseMouseKeyboard.IsPressingEscape };
            lastKeysPressedMatch = new bool[keys_actions_to_exit.Length];
        }

        #endregion Constructors

        #region Methods

        public static void Update(GameTime gameTime)
        {
            for (int i = 0; i < lastKeysPressedMatch.Length; i++)
            {
                // Current key that needs to be pressed to exit
                if (!lastKeysPressedMatch[i])
                {
                    // If first key wasn't pressed
                    if (i == 0)
                    {
                        // Check if this key was pressed
                        if (keys_actions_to_exit[i]())
                        {
                            last_key_press_time = gameTime.TotalGameTime;
                            lastKeysPressedMatch[i] = true;
                        }
                        break;
                    }
                    // If its not the first key to check
                    else
                    {
                        // Check if this key was pressed
                        if (keys_actions_to_exit[i]())
                        {
                            // If activate exit program
                            if (i + 1 == keys_actions_to_exit.Length)
                            {
                                Exit();
                            }
                            else
                            {
                                last_key_press_time = gameTime.TotalGameTime;
                                lastKeysPressedMatch[i] = true;
                            }
                        }
                        // Key wasn't press
                        else
                        {
                            // if key wasn't pressed recently restart the sequence
                            if ((gameTime.TotalGameTime - last_key_press_time).TotalMilliseconds > VALID_MS_BETWEEN_KEY_PRESS)
                            {
                                lastKeysPressedMatch = new bool[keys_actions_to_exit.Length];
                            }
                        }
                        break;
                    }
                }
            }
        }

        public static void Exit()
        {
            game.Exit();
        }

        #endregion Methods
    }
}
