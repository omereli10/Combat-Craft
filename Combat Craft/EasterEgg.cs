﻿using System;
using System.Linq;

namespace Combat_Craft
{
    static class EasterEgg
    {
        #region Data

        private static Player player;
        private static bool[] lastKeysPressedMatch;
        private static Func<bool>[] keyword_actions_for_ea;

        #endregion Data

        #region Constructors

        public static void Initialize(Player player_arg)
        {
            player = player_arg;
            keyword_actions_for_ea = new Func<bool>[] { player.baseMouseKeyboard.IsEasterEgg_o,
                                                        player.baseMouseKeyboard.IsEasterEgg_m,
                                                        player.baseMouseKeyboard.IsEasterEgg_e,
                                                        player.baseMouseKeyboard.IsEasterEgg_r,
                                                        player.baseMouseKeyboard.IsEasterEgg_e,
                                                        player.baseMouseKeyboard.IsEasterEgg_l,
                                                        player.baseMouseKeyboard.IsEasterEgg_i,
                                                        player.baseMouseKeyboard.IsEasterEgg_1,
                                                        player.baseMouseKeyboard.IsEasterEgg_0 };
            lastKeysPressedMatch = new bool[keyword_actions_for_ea.Length];
        }

        public static void Update()
        {
            for (int i = 0; i < lastKeysPressedMatch.Length; i++)
            {
                // Current key that needs to be pressed for the easter egg
                if (!lastKeysPressedMatch[i])
                {
                    if (keyword_actions_for_ea[i]())
                    {
                        // If Easter Egg Activate
                        if (i + 1 == keyword_actions_for_ea.Length)
                        {
                            // Activate the easter egg
                            Globals.easterEggBlockType = Globals.addableBlockTypes[player.baseMouseKeyboard.mouseWheel_Value];
                            Globals.isEasterEggActivated = true;
                            EasterEgg.Activate();
                            
                            // Reset the easter egg
                            lastKeysPressedMatch = new bool[keyword_actions_for_ea.Length];
                        }
                        else
                        {
                            lastKeysPressedMatch[i] = true;
                        }
                    }
                    break;
                }
            }
        }

        #endregion Constructors

        #region Methods

        private static void Activate()
        {
            // Change the chuncks to a certain block value
            GameDictionaries.chunksRenderingDictionary.Keys.ToList().ForEach(k => GameDictionaries.chunksRenderingDictionary[k].BuildBlocksChunk());
            GameDictionaries.blocksAddedDictionary.Keys.ToList().ForEach(k => GameDictionaries.blocksDictionary[k] = Globals.easterEggBlockType);

            // Update the mesh of the chuncks
            GameDictionaries.chunksRenderingDictionary.Keys.ToList().ForEach(k => GameDictionaries.chunksRenderingDictionary[k].BuildChunkMesh());
        }

        #endregion Methods
    }
}
